---

- name: Install PostgreSQL
  become: true
  community.general.apk:
    name: postgresql
    state: present
    update_cache: true

- name: Start PostgreSQL service
  become: true
  ansible.builtin.service:
    name: postgresql
    state: started
    enabled: true

- name: Configure PostgreSQL database
  become: true
  become_user: postgres
  ansible.builtin.shell: |
    psql -c "CREATE ROLE icinga WITH LOGIN PASSWORD '{{ icinga_postgres_password }}'" &&
    createdb -O icinga -E UTF8 icinga &&
    touch /etc/postgresql/.icinga
  args:
    creates: /etc/postgresql/.icinga

- name: Add Icinga authentication to PostgreSQL
  become: true
  become_user: postgres
  ansible.builtin.blockinfile:
    path: /etc/postgresql/pg_hba.conf
    block: |
      # icinga
      local   icinga      icinga                            md5
      host    icinga      icinga      127.0.0.1/32          md5
      host    icinga      icinga      ::1/128               md5

      # "local" is for Unix domain socket connections only
      local   all         all                               ident
      # IPv4 local connections:
      host    all         all         127.0.0.1/32          ident
      # IPv6 local connections:
      host    all         all         ::1/128               ident
  notify: Restart PostgreSQL service

- name: Run pending handlers
  ansible.builtin.meta: flush_handlers

- name: Import Icinga database schema
  ansible.builtin.shell: |
    export PG_PASSWORD={{ icinga_postgres_password }}
    psql -U icinga -d icinga < /usr/share/icinga2-ido-pgsql/schema/pgsql.sql &&
    touch /etc/postgresql/.icinga_schema
  args:
    creates: /etc/postgresql/.icinga

- name: Enable Icinga IDO module for PostgreSQL
  become: true
  ansible.builtin.shell: |
    icinga2 geature enable ido-psql &&
    touch /etc/postgresql/.icinga_ido
  args:
    creates: /etc/postgresql/.icinga_ido
  notify: Restart Icinga service
